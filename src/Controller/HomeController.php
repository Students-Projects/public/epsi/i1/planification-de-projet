<?php

namespace App\Controller;

use App\Entity\Comments;
use App\Entity\Publication;
use App\Entity\User;
use App\Form\CommentaireType;
use App\Form\PublicationFormType;
use DateTime;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function index(Request $request)
    {
        if (!$this->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('app_login');
        }

        $entityManager = $this->getDoctrine()->getManager();
        $date = new DateTime(date("Y-m-d H:i:s"));

        $newPublication = new Publication();
        $newComment = new Comments();

        $newComment->setUser($this->getUser());
        $newPublication->setUser($this->getUser());
        $newComment->setCreationDate($date);
        $newPublication->setCreationDate($date);

        $publications = $entityManager->getRepository(Publication::class)->findAll();
        $commentForm = [];
        $cmtForm = [];

        foreach ($publications as $publi) {
            $commentForm[$publi->getId()] = $this->createForm(CommentaireType::class, $newComment);
            $commentForm[$publi->getId()]->handleRequest($request)->createView();
            $cmtForm[$publi->getId()] = $commentForm[$publi->getId()]->createView();
        }

        $publicationForm = $this->createForm(PublicationFormType::class, $newPublication);
        $publicationForm->handleRequest($request);

        if ($publicationForm->isSubmitted() && $publicationForm->isValid()) {
            $entityManager->persist($newPublication);
            $entityManager->flush();
        }

//        if ($commentForm->isSubmitted() && $commentForm->isValid()) {
//            $newComment->setPublication($entityManager->getRepository(Publication::class)->find($commentForm->get('publication_id')));
//            $entityManager->persist($newComment);
//            $entityManager->flush();
//        }



        return $this->render('home/index.html.twig', [
            'publications' => $publications,
            'publicationForm' => $publicationForm->createView(),
            'commentForm' => $cmtForm,
        ]);
    }

    /**
     * @Route("/publication/like/{id}", name="like_publication")
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function likePublication(Request $request, $id)
    {
        if ($request->getMethod() === "POST") {
            $this->like(Publication::class, $id);
        }
        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/comment/like/{id}", name="like_comment")
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function likeComment(Request $request, $id)
    {
        if ($request->getMethod() === "POST") {
            $this->like(Comments::class, $id);
        }
        return $this->redirectToRoute('home');
    }

    /**
     * @param $class
     * @param $id int
     */
    private function like($class, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $publi = $entityManager->getRepository($class)->find($id);
        $likes = [];
        foreach ($publi->getLikes() as $user) {
            array_push($likes, $user->getId());
        }


        if (in_array($this->getUser()->getId(), $likes)) {
            $publi->removeLike($this->getUser());
        } else {
            $publi->addLike($this->getUser());
        }
        $entityManager->persist($publi);
        $entityManager->flush();
    }
}
